FROM node:12

WORKDIR /app

ENV NODE_PORT=80

RUN echo $NODE_PORT

RUN npm install -g serve

COPY . .
